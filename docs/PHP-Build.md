# PHP Build

Notez que l'utilisation de PHP pour la démonstration est arbitraire, c'est relativement générique, basé sur docker et adaptable à tout langage.

## Build

Considérant qu'il ne s'agit pas d'une application, le build vise à bâtir les images docker à utilisé dans le Pipeline.

Dans le fichier `PHP-Build.gitlab-ci.yml` se trouve la définition des `Jobs` à exécuter et possède les trois `Jobs` suivante:

- [`php:build`](###job-php:build)
- [`php:build:test`](###job-php:build:test)
- [`php:build:failure`](###job-php:build:failure)

### Job `php:build`

Appeler à l'étape `build`, nous utilisons l'image *Docker-in-Docker* pour pouvoir bâtir l'image PHP qui sera utilisé pour le déploiement éventuel d'une application PHP et aussi pour servir de base à l'image de test.

```yml
php:build:
  # Docker in Docker service allow you to use docker cli
  image: docker:19.03.11
  services:
    - docker:19.03.11-dind
  stage: build
  script:
    - echo "FAIL" > BUILD
    - registry_login
    - build_docker -i "$PHP_IMAGE"
  only:
    changes:
      # add some file defining dependencies as package.json
      - docker/Dockerfile
      # - gitlab-ci/Jobs/PHP-Build.gitlab-ci.yml
  artifacts:
    when: on_failure
    expire_in: 5 mins
    paths:
      - BUILD
```

Elle ne sera déclenché que si le fichier `docker/Dockerfile` est altéré et exécutera les scripts d'enregistrement au registre GitLab et construction d'un image ayant le nom défini par la variable `PHP_IMAGE`.

Le `Dockerfile` situé dans le dossier `docker/` est assez simple, on pourrait en profiter pour ajouter des dépendances nécessaire au projet.

`docker/Dockerfile`

```dockerfile
FROM php:7.4

# RUN install some dependencies...
```

Pour la gestion des échecs lors du build, au tout début nous créons le fichier `BUILD`. Ce fichier sera un `artifact` si et seulement si le build échoue.

### Job `php:build:test`

Appeler à l'étape `build:test`, nous utilisons l'image *Docker-in-Docker* pour pouvoir bâtir l'image PHP qui sera utilisé pour exécuter les tests. Puisqu'elle est dépendante de l'image de base `PHP_IMAGE` un autre `stage` à été utilisé au lieu de la démarrer en parallèle à l'étape `build`.

```yml
php:build:test:
  # Docker in Docker service allow you to use docker cli
  image: docker:19.03.11
  services:
    - docker:19.03.11-dind
  stage: build:test
  script:
    - echo "FAIL" > BUILD
    - registry_login
    # Dockerfile take $PHP_IMAGE as build argument
    # so it will be FROM $PHP_IMAGE:latest
    # to ensure all project dependencies added to PHP_IMAGE
    - build_docker -i "$PHP_TEST_IMAGE" -d "Test.Dockerfile" -a "base_image=$PHP_IMAGE:latest"
  only:
    changes:
      - docker/Dockerfile
      - docker/Test.Dockerfile
  artifacts:
    when: on_failure
    expire_in: 5 mins
    paths:
      - BUILD
```

Elle ne sera déclenché que si le fichier `docker/Test.Dockerfile` est altéré et exécutera les scripts d'enregistrement au registre GitLab et construction d'un image ayant le nom défini par la variable `PHP_TEST_IMAGE`. Puisque le nom du fichier est custom, nous lui passeront son nom via l'argument `-d` qui sera ensuite utilisé par l'argument `--file` de la commande `docker build`.

Le `Test.Dockerfile` situé dans le dossier `docker/` est un peu plus complexe et utilise la notion d'argument. On défini des arguments dans un `Dockerfile` de la façon suivante:

```dockerfile
ARG var[=default_value]
```

Et peut ensuite être utilisé `RUN echo ${var}`.

Dans le cas de `Test.Dockerfile`, nous gardons la possibilité de modifié l'image de base sur laquelle se construira notre image de test, nous pouvons donc tester notre code avec différente version de PHP ou la tester en utilisant notre image `PHP_IMAGE` ayant les dépendances requises. La chaîne de caractères représentant les arguments sera passer à notre fonction `build_docker` via l'argument `-a` et sera transféré à l'argument `--build-arg` de la commande `docker build` tel que dans « `build_docker -i "$PHP_TEST_IMAGE" -d "Test.Dockerfile" -a "base_image=$PHP_IMAGE:latest"` ».

`docker/Test.Dockerfile`

```dockerfile
ARG base_image=php:7.4

FROM ${base_image}

RUN set -xe

# Install git (the php image doesn't have it) which is required by composer
RUN apt-get update -yqq
RUN apt-get install git -yqq

# Install phpunit, the tool that we will use for testing
RUN curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
RUN chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
# `docker-php-ext-install` is a script provided by the official PHP Docker image
# that you can use to easily install extensions.
# For more information read the documentation at https://hub.docker.com/_/php
RUN docker-php-ext-install pdo_mysql
```

Pour la gestion des échecs lors du build, au tout début nous créons le fichier `BUILD`. Ce fichier sera un `artifact` si et seulement si le build échoue.

### Job `php:build:failure`

Appeler à l'étape `build:failure` si et seulement si une étape préalable de build a échoué. Elle pourrait être utilisé pour faire un rollback de certaine action ou pour envoyer un message via Slack, Email, Hub.

```yml
php:build:failure:
  image: alpine:latest
  stage: build:failure
  script:
    - cat BUILD && chmod +x ./gitlab-ci/Scripts/on_build_failure.sh && sh gitlab-ci/Scripts/on_build_failure.sh || echo "Not a build error"
  when: on_failure
```

Pour savoir si l'étape de build à échoué, elle tente d'accéder au fichier créé, si elle y arrive elle exécute un script présent dans `gitlab-ci/Scripts/on_build_failure.sh`, sinon c'est qu'il ne s'agissait pas d'un erreur lors du build.
