# PHP Deploy

Notez que l'utilisation de PHP pour la démonstration est arbitraire, c'est relativement générique, basé sur docker et adaptable à tout langage.

## Deploy

Dans le fichier `PHP-Deploy.gitlab-ci.yml` se trouve la définition des `Jobs` à exécuter et possède les trois `Jobs` suivante:

- [`chatbot:deploy`](###job-chatops:deploy)
- [`php:deploy`](###job-php:deploy)
- [`php:deploy:failure`](###job-php:deploy:failure)

### Job `chatops:deploy`

Appeler à l'étape `deploy`, cette mécanique sera déclenché en parallèle de la `Job` `php:deploy`. Elle sert à envoyer un message pour aviser que les builds et tests précédent ont réussis et que le déploiement est prêt à être déclenché manuellement. Il est aussi possible que se message soit interactif par l'utilisation d'un bouton dans Slack ou par Courriel qui déclenchera la `Job` manuelle de déploiement.

```yml
chatops:deploy:
  stage: deploy
  script:
    - echo "Execute some chatbot to ask manual deploy"
  when: on_success
```

Le point important est le `when: on_success` qui indique que cette job sera exécuté seulement si toutes les jobs précédentes sont réussis. C'est de cette façon qu'on peu détecter le succès des builds et des tests.

### Job `php:deploy`

Appeler à l'étape `deploy`, cette mécanique sera déclenché en parallèle de la `Job` `chatops:deploy`, mais sera en attente d'une interaction manuelle pour se déclencher. Elle sert à effectuer les processus de déploie de notre application. Le déclenchement peut se faire via L'interface de GitLab ou via l'[API](https://docs.gitlab.com/ee/api/jobs.html#play-a-job)

```yml
php:deploy:
  stage: deploy
  script:
    - echo "Triggered by chatbot response"
  when: manual
```

### Job `php:deploy:failure`

Appeler à l'étape `deploy:failure` si et seulement si le déploiement a échoué. Elle pourrait être utilisé pour faire un rollback de certaine action ou pour envoyer un message via Slack, Email, Hub.

```yml
php:deploy:failure:
  image: alpine:latest
  stage: deploy:failure
  script:
    - cat DEPLOY && chmod +x ./gitlab-ci/Scripts/on_deploy_failure.sh && ./gitlab-ci/Scripts/on_deploy_failure.sh || echo "Not a deploy error"
  when: on_failure
```

Pour savoir si l'étape de déploiement à échoué, elle tente d'accéder au fichier créé, si elle y arrive elle exécute un script présent dans `gitlab-ci/Scripts/on_deploy_failure.sh`, sinon c'est qu'il ne s'agissait pas d'un erreur lors du déploiement.
