# PHP Unit Test

Notez que l'utilisation de PHP pour la démonstration est arbitraire, c'est relativement générique, basé sur docker et adaptable à tout langage.

## Test

Dans le fichier `PHP-Unit-Test.gitlab-ci.yml` se trouve la définition des `Jobs` à exécuter et possède les trois `Jobs` suivante:

- [`.php:test`](###job-php:test)
- [`php:test:unit`](###job-php:test:unit)
- [`php:test:integration`](###job-php:test:integration)
- [`php:test:functional`](###job-php:test:functional)
- [`php:test:failure`](###job-php:test:failure)

### Job `.php:test`

Il ne s'agit pas ici d'une `Job` à proprement dit, c'est une partie de `Job` qui peut être utilisé au travers d'autres `Jobs` sous le principe d'héritage via la clef `extends`. Dans ce cas il s'agit uniquement de prédéfinir l'image pour toutes les `Jobs` héritant de celle-ci, mais on aurait pu inclure des variables et des scripts qui se serait exécuté avant ceux de la `Job` dérivé.

```yml
.php:test:
  image: $PHP_TEST_IMAGE:latest
```

### Job `php:test:unit`

Appeler à l'étape `test:unit`, nous dérivons de la `Job` partiel `.php:test`. Elle exécute les tests unitaires préalablement créé. Ma connaissance de `PHP` étant limité les appel sont hardcodé mais aurait pu être configuré de façon générique via un fichier `XML` ce qui aurait rendu ce segment applicable à tout code `PHP`.

```yml
php:test:unit:
  extends: .php:test
  stage: test:unit
  script:
    - phpunit --bootstrap ./src/Calculator.php ./tests/CalculatorTest.php
    - phpunit --bootstrap ./src/Email.php ./tests/EmailTest.php
  allow_failure: false
```

### Job `php:test:integration`

Appeler à l'étape `test:integration`, nous dérivons de la `Job` partiel `.php:test`. Elle exécute les tests d'intégrations préalablement créé. Un test d'intégration vise à s'assuré de la communication entre différent module et service tel qu'un API ou un Base de données. Ma connaissance de `PHP` étant limité les appel sont hardcodé mais aurait pu être configuré de façon générique via un fichier `XML` ce qui aurait rendu ce segment applicable à tout code `PHP`.

```yml
php:test:integration:
  extends: .php:test
  stage: test:integration
  script:
    - phpunit --bootstrap ./src/Music.php ./tests/MusicIntegrationTest.php
  allow_failure: false
```

Cette `Job` aurait pu être exécuté en parallèle sur le même stage plutôt qu'à un stage subséquent, mais je désirais m'assuré qu'elle ne s'exécute pas si le test précédent échouait.

### Job `php:test:functional`

Appeler à l'étape `test:functional`, nous dérivons de la `Job` partiel `.php:test`. Elle exécute les tests de fonctionnement préalablement créé. Un test de fonctionnement vise à s'assuré que le traitement le traitement des données récupéré d'un module ou service autre sont tel qu'attendu. Ma connaissance de `PHP` étant limité les appel sont hardcodé mais aurait pu être configuré de façon générique via un fichier `XML` ce qui aurait rendu ce segment applicable à tout code `PHP`.

```yml
php:test:functional:
  extends: .php:test
  stage: test:functional
  script:
    - phpunit --bootstrap ./src/Music.php ./tests/MusicFunctionalTest.php
  allow_failure: false
```

Cette `Job` aurait pu être exécuté en parallèle sur le même stage plutôt qu'à un stage subséquent, mais je désirais m'assuré qu'elle ne s'exécute pas si le test précédent échouait.

### Job `php:test:failure`

Appeler à l'étape `test:failure` si et seulement si une étape préalable de test a échoué. Elle pourrait être utilisé pour faire un rollback de certaine action ou pour envoyer un message via Slack, Email, Hub.

```yml
php:test:failure:
  image: alpine:latest
  stage: test:failure
  script:
    - cat TEST && chmod +x ./gitlab-ci/Scripts/on_test_failure.sh && sh gitlab-ci/Scripts/on_test_failure.sh || echo "Not a test error"
  when: on_failure
```

Pour savoir si l'étape de test à échoué, elle tente d'accéder au fichier créé, si elle y arrive elle exécute un script présent dans `gitlab-ci/Scripts/on_test_failure.sh`, sinon c'est qu'il ne s'agissait pas d'un erreur lors d'un test.
